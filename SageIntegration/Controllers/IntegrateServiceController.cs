﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using Sage.Peachtree.API;
using System.Configuration;

namespace SageIntegration.Controllers
{
    public class IntegrateServiceController : ApiController
    {
        [HttpPost]
        public string GenerateInvoice([FromBody]string invoiceAsJSON)
        {
            dynamic invoice = Json.Decode(invoiceAsJSON);
            var invoiceItems = invoice.OrderItems;

            try
            {
                var newSession = CreateNewSession();
                var newCompany = CreateCompany(newSession);

                var accountReferenceKey = RetrieveAccountRecord(newCompany, invoice);
                var customerReferenceKey = RetrieveCustomerRecord(newCompany, accountReferenceKey, invoice);

                var accountFactory = newCompany.Factories.AccountFactory;
                var accountListing = accountFactory.List();
                accountListing.Load();
                var existingAccount = accountListing.FirstOrDefault(singleAccount => singleAccount.ID == invoice.AccountId);

                var customerFactory = newCompany.Factories.CustomerFactory;
                var customerList = customerFactory.List();
                customerList.Load();
                var exisitngCustomer = customerList.FirstOrDefault(singleCustomer => singleCustomer.ID == invoice.AccountId);
                
                var salesOrderFactory = newCompany.Factories.SalesOrderFactory;
                var newSalesOrder = salesOrderFactory.Create();

                newSalesOrder.AccountReference = existingAccount.Key;
                newSalesOrder.CustomerReference = exisitngCustomer.Key;
                newSalesOrder.Date = Convert.ToDateTime(invoice.OrderDate.ToString());
                newSalesOrder.ShipByDate = Convert.ToDateTime(invoice.ShipBy.ToString());
                newSalesOrder.ShipVia = invoice.ShipVia;
                newSalesOrder.ReferenceNumber = invoice.ReferenceNumber;
                newSalesOrder.CustomerPurchaseOrderNumber = invoice.PONumber;

                newSalesOrder.CustomerNote = "CONTACT: " + invoice.ContactFirstName + " " + invoice.ContactLastName + "\n"
                    + "EMAIL: " + invoice.CustomerEmail + "\n" + invoice.Notes;
                newSalesOrder.ShipToAddress.Address.Address1 = invoice.ShippingAddressLine1;
                newSalesOrder.ShipToAddress.Address.City = invoice.ShippingCity;
                newSalesOrder.ShipToAddress.Address.State = invoice.ShippingState;
                newSalesOrder.ShipToAddress.Address.Zip = invoice.ShippingZipCode;
                newSalesOrder.ShipToAddress.Address.Country = invoice.ShippingCountry;
                newSalesOrder.TermsDescription = invoice.Terms;

                var employeeFactory = newCompany.Factories.EmployeeFactory;
                var employeeList = employeeFactory.List();
                employeeList.Load();

                var employeeToRetrieve = employeeList.FirstOrDefault(employee => employee.ID == (invoice.RepName));
                if(employeeToRetrieve != default(Employee))
                    newSalesOrder.SalesRepresentativeReference = employeeToRetrieve.Key;

                HashSet<String> inventoryIdSet = new HashSet<String>();
                foreach (dynamic item in invoiceItems)
                    inventoryIdSet.Add(item.ProductCode);

                var inventoryFactory = newCompany.Factories.InventoryItemFactory;
                var inventoryList = inventoryFactory.List();
                inventoryList.Load();

                Dictionary<String, InventoryItem> inventoryIdToInventoryItem = new Dictionary<String, InventoryItem>();
                List<InventoryItem> inventory = inventoryList.Where(item => inventoryIdSet.Contains(item.ID)).ToList();
                    
                foreach (var item in inventory)
                {
                    inventoryIdToInventoryItem.Add(item.ID, item);
                }

                foreach (dynamic item in invoiceItems)
                {
                    var orderItem = newSalesOrder.AddLine();
                    orderItem.AccountReference = existingAccount.Key;
                    orderItem.Amount = item.Amount;
                    orderItem.Description = item.ProductDescription;
                    orderItem.Quantity = item.Quantity;
                    orderItem.UnitPrice = item.UnitPrice;

                    if (inventoryIdToInventoryItem.ContainsKey(item.ProductCode))
                        orderItem.InventoryItemReference = inventoryIdToInventoryItem[item.ProductCode].Key;
                }
                
                newSalesOrder.Save();
                
                if (newSession.SessionActive)
                    newSession.End();
            }
            catch (Exception failedToInstantiate)
            {
                return "An exception has been thrown from Sage DB-1." + failedToInstantiate.StackTrace + " : " + failedToInstantiate.Message; 
            }

            var responseForUser = new { success = true };
            return Json.Encode(responseForUser);
        }

        private EntityReference<Customer> RetrieveCustomerRecord(Company currentCompany, EntityReference<Account> accountingKey, dynamic invoiceWithCustomer)
        {
            var customerFactory = currentCompany.Factories.CustomerFactory;
            var customerList = customerFactory.List();
            customerList.Load();

            var customerToRetrieve = customerList.FirstOrDefault(singleCustomer => singleCustomer.ID == invoiceWithCustomer.AccountId);

            if (customerToRetrieve != default(Customer))
                return customerToRetrieve.Key;
            
            var newCustomer = customerFactory.Create();
            newCustomer.Email = invoiceWithCustomer.CustomerEmail;
            newCustomer.Name = invoiceWithCustomer.CustomerName;
            newCustomer.ID = invoiceWithCustomer.ContactId;

            var newContact = newCustomer.Contacts.First();

            newContact.FirstName = invoiceWithCustomer.ContactFirstName;
            newContact.LastName = invoiceWithCustomer.ContactLastName;
            newContact.Address.Address1 =invoiceWithCustomer.ContactAddressLine1;
            newContact.Address.City = invoiceWithCustomer.ContactCity;
            newContact.Address.State = invoiceWithCustomer.ContactState;
            newContact.Address.Zip = invoiceWithCustomer.ContactZipCode;
            newContact.Address.Country = invoiceWithCustomer.ContactCountry;
            newContact.CompanyName = invoiceWithCustomer.CustomerName;

            newCustomer.Save();
            return newCustomer.Key;
        }

        private EntityReference<Account> RetrieveAccountRecord(Company currentCompany, dynamic inoviceWithAccount)
        {
            var accountFactory = currentCompany.Factories.AccountFactory;
            var accountListing = accountFactory.List();
            accountListing.Load();
            var accountToRetrieve = accountListing.FirstOrDefault(singleAccount => singleAccount.ID == inoviceWithAccount.AccountId);

            if (accountToRetrieve != default(Account))
                return accountToRetrieve.Key;
            var newAccount = accountFactory.Create();
            newAccount.Classification = AccountClassification.Payable;
            newAccount.Description = inoviceWithAccount.AccountDesciption;
            newAccount.ID = inoviceWithAccount.AccountId;

            newAccount.Save();

            return newAccount.Key;
        }

        private PeachtreeSession CreateNewSession()
        {
            var newSession = new PeachtreeSession();

            try
            {    
                newSession.Begin(ConfigurationManager.AppSettings["ApplicationIdentity"]);
            }
            catch (Exception failureToCreateSession) { throw failureToCreateSession; }
            return newSession;
        }

        private Company CreateCompany(PeachtreeSession newSession)
        {
            CompanyIdentifier companyIdentity = null;
            Company companyToCreate = null;

            try
            {
                foreach (var singleCompany in newSession.CompanyList())
                {
                    if (singleCompany.DatabaseName.Equals(ConfigurationManager.AppSettings["SageDatabaseName"]))
                    {
                        companyIdentity = singleCompany;
                        break;
                    }
                }

                var authorizationInformation = AuthorizationResult.Pending;

                int triesToConnectToSage = 0;
                while ((authorizationInformation == AuthorizationResult.Pending) && triesToConnectToSage < 10)
                {
                    authorizationInformation = newSession.RequestAccess(companyIdentity);
                    System.Threading.Thread.Sleep(1000);
                    triesToConnectToSage++;
                }

                companyToCreate = newSession.Open(companyIdentity);
            }
            catch (Exception failureToCreateCompany) 
            {
                throw failureToCreateCompany; 
            }

            return companyToCreate;
        }
    }
}
